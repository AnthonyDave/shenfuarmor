from __future__ import print_function
import numpy as np
import argparse
import cv2 as cv


parser = argparse.ArgumentParser(description='This program shows how to use background subtraction methods provided by \
                                             OpenCV. You can process both videos and images.')
parser.add_argument('--input', type=str, help='Path to a video or a sequence of image.', default='./pictures/806497368886.avi')
parser.add_argument('--algo', type=str, help='Background subtraction method (KNN, MOG2).', default='MOG2')
args = parser.parse_args()
if args.algo == 'MOG2':
    backSub = cv.createBackgroundSubtractorMOG2()
else:
    backSub = cv.createBackgroundSubtractorKNN()
cap = cv.VideoCapture(cv.samples.findFileOrKeep(args.input))

while(cap.isOpened()):
    ret, frame = cap.read()
 # 进行高斯模糊
    blurred = cv.GaussianBlur(frame, (11, 11), 0)
    # 转换颜色空间到HSV
    hsv = cv.cvtColor(blurred, cv.COLOR_BGR2HSV)
    # 定义色无图的HSV阈值
    lower_red = np.array([35, 43, 46])  
    upper_red = np.array([155, 255, 255])  
    # 对图片进行二值化处理
    mask = cv.inRange(hsv, lower_red, upper_red)
    # 腐蚀操作
    mask = cv.dilate(mask, None, iterations=6)
    mask = cv.erode(mask, None, iterations=8)
    # 膨胀操作，先腐蚀后膨胀以滤除噪声
    

    fgMask = backSub.apply(mask)

    mask1 = np.zeros(fgMask.shape,np.uint8)
    cnts = cv.findContours(fgMask.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)[-2]



#    if len(cnts) > 100000 and len(cnts) < 200000:
    if len(cnts) > 0:
    #cv.boundingRect()返回轮廓矩阵的坐标值，四个值为x, y, w, h， 其中x, y为左上角坐标，w,h为矩阵的宽和高
        boxes = [cv.boundingRect(c) for c in cnts]
        for box in boxes:
            x, y, w, h = box
        #绘制矩形框对轮廓进行定位
            if w>13 and h>13 and w<70 and h<70:
                origin_pic = cv.rectangle(mask1, (x, y), (x+w, y+h), (153, 153, 0), 2)
             
    cv.imshow('roi', mask1)
#    cv.imshow('fg', fgMask)
    dst=cv.cornerHarris(mask1,2,3,0.04)
    frame[dst>0.01*dst.max()]=[0,255,0]
	
    cv.imshow('frame', frame)
    
    if cv.waitKey(1) & 0xFF == ord('q'):
        break

#frame.release()
cap.release()
cv.destroyAllWindows()
